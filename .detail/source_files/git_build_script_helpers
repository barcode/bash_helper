#!/bin/bash

script-helper-build-git-repo-pre-make() #name version url [packages]
{
    ############################################################################
    #params
    name="$1"
    ver="$2"
    original_ver="$2"
    url="$3"
    packages="${@:4}"
    #TODO check params
    ############################################################################
    #base vars
    export base_dir="$(pwd)"
    export clone_dir="$base_dir/$name"
    export build_dir_base="/tmp/$(whoami)/$base_dir/build"
    export build_dir="/$build_dir_base/$ver"
    if ccache -p | grep -q "(default) cache_dir =" && [ -z "$CCACHE_DIR" ]
        then export CCACHE_DIR="/tmp/$(whoami)/ccache"
    fi

    export source_me="$base_dir/source-${name}s"
    touch-bash-source-script "$source_me"
    mkdir -p "$base_dir/install"
    ############################################################################
    #packages
    [ -z "$packages" ] || script-helper-are-all-installed $packages
    ############################################################################
    #clone / update
    [ -e "$clone_dir" ] || git clone --recursive "$url" "$clone_dir"

    cd "$clone_dir"
    if [ -z "$ver" ]
    then
        LRED echo "no version specified! either use a tag or branch name"
        LBLUE echo "tags:"
        git tag | cat
        LBLUE echo "branches:"
        git abranch | cat
        LRED echo "no version specified! either use a tag or branch name"
        exit 1
    fi
    git fetch -p
    ############################################################################
    #clean_clone_dir
    clean_code_dir()
    {
        changes_r="$(                      git status -bs | grep -Ev '^(#|Entering)' || true)"
        changes_s="$(git submodule foreach git status -bs | grep -Ev '^(#|Entering)' || true)"
        if [ "$(echo -n "$changes_r" | wc -c)" -gt 0 ] || [ "$(echo -n "$changes_s" | wc -c)" -gt 0 ]
        then
            rm -rf "$clone_dir/"*
            git checkout .
            git submodule update --init
        fi
    }
    clean_code_dir

    try_checkout_and_recover()
    {
        ! git checkout "$@" || return 0
        #checkout failed! -> remove any
        git checkout "$@"                           |&  \
            grep -v "^error: "                      |   \
            grep -v "^Please move or remove them "  |   \
            grep -v "^Aborting"                     |   \
            sed -E 's|.[\t ]*(.*)|\1|g'             |   \
            xargs -I% rm "$PWD/%"
        clean_code_dir
        ! git checkout "$@" || return 0
        #checkout failed again! -> more aggressive clean
        rm -rf "$clone_dir/"*
        git checkout .
        git submodule update --init
        ! git checkout "$@" || return 0
        #checkout failed again! -> exit
        LRED errcho "checking out fails! params: $@"
        exit 1
    }
    ############################################################################
                                    if false
                                    then
                                        #checkout
                                        is_tag=false
                                        is_branch=false
                                        is_commit=false
                                        ######################################
                                        if git tag          | grep "^$ver$"
                                            then is_tag=true
                                            LGREEN echo "found rag $ver"
                                        elif git branch --all | grep -q "^[* ]*remotes/origin/$ver$"
                                        then
                                            is_branch=true
                                            LGREEN echo "found branch $ver"
                                            branch_name="$ver"
                                            ver="$(git chash)"

                                            cd "$base_dir/install"
                                            install_branch_link="$base_dir/install/$branch_name"
                                            rmlnk "$install_branch_link" |& true
                                            ln -s "$ver" "$branch_name"
                                            cd -
                                        elif [ "$(git cat-file -t "$ver")" == commit ]
                                        then
                                            is_commit=true
                                            LGREEN echo "found commit $ver"
                                            ver="$(git chash)"
                                        fi
                                    fi
    ######################################
    cd "$clone_dir"
    if ! git hard-reset-checkout "$ver"
    then
        LRED errcho "unknown version: $ver"
        exit 1
    fi
    if git branch --all | grep -q "^[* ]*remotes/origin/$ver$"
    then
        branch_name="$ver"
        ver="$(git chash)"

        cd "$base_dir/install"
        install_branch_link="$base_dir/install/$branch_name"
        rmlnk "$install_branch_link" |& true
        ln -s "$ver" "$branch_name"
        cd -
    fi
    install_dir="$base_dir/install/$ver"
    ############################################################################
    #checkout
    if [ -e "$install_dir" ]
    then
        LBLUE echo "$name $ver is already installed in $install_dir"
        exit
    fi
    ############################################################################
    #build
    [ -e "$build_dir_base" ] && rm -rf "$build_dir_base"
    mkcd "$build_dir"
    ############################################################################
    #packages
    [ -z "$packages" ] || script-helper-are-all-installed $packages
}
export -f script-helper-build-git-repo-pre-make

make-and-analize-error-no-exit() #n
{
    n=1
    [ -z "$1" ] || n=$1
    return=1
    i=0
    while [ $i -lt $n ] && ! [ $return -eq 0 ]
    do
        i=$(($i + 1))
        make |& tee make-output-$i
        return=${PIPESTATUS[0]}
    done

    if ! [ $return -eq 0 ]
    then
        LRED errcho -e "make failed (exit = $return)\nnow trying to find errors"
        patterns="cd|ln|rm|make|mv|cp"
        patterns="$patterns|((CCACHE_SLOPPINESS=.* )?ccache )?(gcc|g++)"
        patterns="$patterns|$ArmarX_DIR/build-qt5/install/"
        patterns="$patterns|$(pwd | sed 's|//|/|g')"
        patterns="$patterns|wayland-scanner "
        patterns="$patterns|.*\\[-W(unused-(parameter|result)|sign-compare)\\]$"
        grep -vnE "^($patterns)" make-output-1 | highlight-output -iE "(error|fail)"
        echo "filter out patterns: $patterns"

        LRED errcho -e "make failed (exit = $return)\nfull log in $PWD/make-output-1"
    fi
    return $return
}
export -f make-and-analize-error-no-exit

make-and-analize-error() #n
{
    make-and-analize-error-no-exit "$@"
    [ $? -eq 0 ] || exit 1
}
export -f make-and-analize-error
